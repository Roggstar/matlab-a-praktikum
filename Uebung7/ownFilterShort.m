function [y] = ownFilterShort(N,g,x)

    y = zeros(length(x),1);
    for n=1:length(x)
        if(n-N >= 1)
            y(n) = g.*x(n) + x(n-N) - g.*y(n-N);
        else
            y(n) = g.*x(n);
        end
    end