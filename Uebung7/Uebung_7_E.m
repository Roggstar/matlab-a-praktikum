clear;
close all;
clc;

%% Aufgabe 1
[audioSignal, Fs] = audioread('Manhat10Fine.wav');

%% Aufgabe 2
y = ownFilterShort(5000,0.7,audioSignal);
y = ownFilterShort(5000,0.7,y);
y = ownFilterShort(5000,0.7,y);
y = ownFilterShort(5000,0.7,y);
y = ownFilterShort(5000,0.7,y);

y = y + audioSignal;

y_norm = y./max(abs(y));

sound(y_norm,Fs);