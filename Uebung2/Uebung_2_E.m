close all;
clear all;
clc;

s = rand(1,10000);
s = round(s)*2.-1;

pn = pnseq_gen(10);
pn = pn*2.-1;
x = [pn s];

c = rand();
variance = 0.25;
disp(['c betr�gt: ' num2str(c)]);
y = kanaluebertragung(x, c, variance);

c_Geschaetzt = kanalSchaetzer(y,pn);
disp(['c_Geschaetzt betr�gt: ' num2str(c_Geschaetzt)]);

x_Geschaetzt = y/c_Geschaetzt;
s_Geschaetzt = x_Geschaetzt(length(pn)+1:end);

MSE = sum((s-s_Geschaetzt).^2)/length(s);
disp(['Der MSE betr�gt: ' num2str(MSE)]);

variances = linspace(0,0.5,20);
MSEs = [];
analyticalMSEs = [];
for i=1:20
    y = kanaluebertragung(x, c, variances(i));
    c_Geschaetzt = kanalSchaetzer(y,pn);
    x_Geschaetzt = y/c_Geschaetzt;
    s_Geschaetzt = x_Geschaetzt(length(pn)+1:end);
    newMSE = sum((s-s_Geschaetzt).^2)/length(s);
    MSEs = [MSEs newMSE];
    newAnalyticalMSE = variances(i)/c_Geschaetzt^2;
    analyticalMSEs = [analyticalMSEs newAnalyticalMSE];
end
perfectMSE = variances./c^2;
stem(variances, MSEs, 'red');
hold on;
stem(variances, analyticalMSEs, 'blue', 'x');
plot(variances, perfectMSE, 'green');
legend('MSE', 'analytical MSE', 'perfect MSE');
xlabel('Variance');
ylabel('MSE');