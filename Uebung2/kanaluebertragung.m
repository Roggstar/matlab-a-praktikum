function [y] = kanaluebertragung(x, c, NoiseVar)

    standardDeviation = sqrt(NoiseVar);
    n = standardDeviation.*randn(1,length(x));
    y = c.*x + n;
    Varianz = var(n);
    disp(['Die Varianz des Rauschens betr�gt: ' num2str(Varianz)]);
end