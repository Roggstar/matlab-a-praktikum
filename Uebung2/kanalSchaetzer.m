function [c_Geschaetzt] = kanalSchaetzer(y,pn)

    crossCorr = xcorr(y,pn);
    maximum = max(crossCorr);
    c_Geschaetzt = maximum/length(pn);

end

