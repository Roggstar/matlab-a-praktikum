%This code generates PN sequence and is written by Ashwini Patankar. 
%You can reach me through ashwinispatankar@gmail.com 
%My Home page: www.ashwinipatankar.com
%My Personal Blog: wwww.ashinipatankar.wordpress.com
%Blog on Wireless Engineering : www.wirelesscafe.wordpress.com
% Feel free to ask querries related to wirelss engineering, matlab and ns2
function [op_seq] = pnseq_gen(a)
% a : no of fliflops; b = tapp _ function starting frm highest order; c = initial stae
%tapp functions or genrator polynomial
%e.g. no of flip flops 4 ==> a = 4
%generator polynomial x4+x+1    ==> b = [1 0 0 1 1]
%initial state [1 0 0 0] ==> c = [1 0 0 0]
%refere figure to set a relation between tap function and initial state
%
%
%
%
%                <   
%   -----------X____________________________________
%  |     ___    |    _____   ____        _____       |   
%  |>   |   |   |    |   |   |   |       |   |      ^|
%  -----|   |--------|   |---|   |-------|   |------------->o/p
%       -----       -----   -----       ----
%         x1 +         x2   + x3     +    x4
%initial state
%       1               0       0           1
%
%take care of the reverse order of genrator polynomial and intiial states
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% close all;
% clc;
switch a
    case 2
        b=[1 1 1];
        c=[1 0 ];
    case 3
        b=[1 0 1 1];
        c=[1 0 0];
    case 4
        b=[1 0 0 1 1];
        c=[1 0 0 0];
    case 5
        b=[1 0 0 1 0 1];
        c=[1 0 0 0 0];
    case 6
        b=[1 0 0 0 0  1 1];
        c=[1 0 0 0 0 0];
    case 7
        b=[1 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0];
    case 8
        b=[1 0 1 1 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0];
    case 9
         b=[1 0 0 0 0 1 0 0 0 1];
        c=[1 0 0 0 0 0 0 0 0];
    case 10
         b=[1 0 0 0 0 0 0 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0];
    case 11
         b=[1 0 0 0 0 0 0 0 0 1 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0];
    case 12
         b=[1 0 0 0 0 1 0 0 1 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0];
    case 13
        b=[1 0 0 0 0 0 0 0 0 1 1 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0];
    case 14
        b=[1 0 1 1 0 0 0 0 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 15
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 16
        b=[1 0 0 0 0 0 0 0 0 0 0 1 0 1 1 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 17
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 18
        b=[1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 19
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 20
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 21
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 22
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 23
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 24
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 25
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 26
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 27
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 28
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 29
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    case 30
        b=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1];
        c=[1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
    otherwise
end
 x = a;
tap_ff =b;
int_stat= c;
for i = 1:1: length(int_stat)
    old_stat(i) = int_stat(i);
    gen_pol(i) = tap_ff(i);
end
len = (2 ^x)-1;
gen_pol(i+1)= 1;
gen_l = length(gen_pol);
old_l = length(old_stat);
for i1 = 1: 1:len
    % feed back input genration
    t = 1;
    for i2 = 1:old_l
        if gen_pol(i2)==1
            stat_str(t) = old_stat(gen_l - i2);
            i2 = i2+1;
            t = t+1;
        else
            i2 = i2+1;
        end
    end
    stat_l = length(stat_str);
    feed_ip = stat_str(1);
    for i3 = 1: stat_l-1
        feed_ip = bitxor(feed_ip,stat_str(i3 + 1)); 
        feed_ipmag(i1) = feed_ip;
        i3 = i3+1;
    end
    % shifting elements
    new_stat = feed_ip;
    for i4 = 1:1:old_l
        new_stat(i4+1) = old_stat(i4);
        old_stat(i4)= new_stat(i4);
    end
    op_seq(i1) = new_stat(old_l +1);
end
%op_seq;
    