clear;
close all;
clc;

%% Aufgabe 1
laenge = 150000;
b = randi([0 1],1,laenge);

s = modulation(b,'QPSK');

%% Aufgabe 2

X = Alamouti_encoder(s);

%% Aufgabe 3

standard_deviation = sqrt(0.5);
H1 = standard_deviation.*randn([1 laenge/2]) + 1i.*standard_deviation*randn([1 laenge/2]);
H1(2:2:end) = H1(1:2:end);
H2 = standard_deviation.*randn([1 laenge/2]) + 1i.*standard_deviation*randn([1 laenge/2]);
H2(2:2:end) = H2(1:2:end);
H = [H1;H2];

%% Aufgabe 4

Eb_N0_dB = linspace(-10,25,10);

%% Aufgabe 5 a/b/c
variance = 0.5;
standard_deviation = sqrt(variance);
n = standard_deviation*randn([1 laenge/2]) + 1i*standard_deviation*randn([1 laenge/2]);
y = [];
BERs = zeros(1,10);
for k=1:10
    a = sqrt(10^(Eb_N0_dB(k)/10)/2);
    y = [y; a*dot(conj(H(:,1:end)),X(:,1:end)) + n];
    s_dach = Alamouti_decoder(y(k,:),H);
    b_schlange = demodulation(s_dach,'QPSK');
    BERs(k) = length(find(not(b_schlange==b)))/length(b);
end

%% Aufgabe 6
H_SISO = [H1;zeros(1,laenge/2)];
BERs_SISO = zeros(1,10);
y_SISO = [];
for k=1:10
    a = sqrt(10^(Eb_N0_dB(k)/10));
    y_SISO = [y_SISO; a*dot(conj(H_SISO(:,1:end)),X(:,1:end)) + n];
    s_dach_SISO = Alamouti_decoder(y_SISO(k,:),H_SISO);
    b_schlange_SISO = demodulation(s_dach_SISO,'QPSK');
    BERs_SISO(k) = length(find(not(b_schlange_SISO==b)))/length(b);
end

%%Aufgabe 7
roh = 10.^(Eb_N0_dB./10);
P = 0.5 - 0.5*sqrt((1.+2./roh)).^-1;
BERs_analytical = P.^2.*(1.+2*(1.-P));
semilogy(Eb_N0_dB,BERs);
hold on;
semilogy(Eb_N0_dB,BERs_SISO);
semilogy(Eb_N0_dB,BERs_analytical);
grid on;
title('Bit error rate for SISO, MISO and analytical');
xlabel('Eb/N0 in dB');
ylabel('BER');
legend('BER MISO', 'BER SISO', 'BER analytical');
