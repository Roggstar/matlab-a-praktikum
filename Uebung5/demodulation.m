function [b_schlange] = demodulation(y, modulationsArt)

    switch modulationsArt
        case 'BPSK'
            b_schlange = sign(y);
            b_schlange(find(b_schlange <= 0)) = 0;
        case 'QPSK'
            for k = 1:2:(length(y)*2)
                if (imag(y((k+1)/2)) <= 0)
                    b_schlange(k) = 1;
                else
                    b_schlange(k) = 0;
                end
                
                if (real(y((k+1)/2)) <= 0)
                    b_schlange(k+1) = 1;
                else
                    b_schlange(k+1) = 0;
                end
            end
    end
end

