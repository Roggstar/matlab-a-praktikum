function [s_dach] = Alamouti_decoder(y,H)

    s_dach = zeros(1,size(y,2));
    for k=0:size(y,2)/2-1
        g_1 = H(1,2*k+1);
        g_2 = H(2,2*k+1);
        s_dach(2*k+1:2*k+2) = 1/(abs(g_1)^2+abs(g_2)^2)*[conj(g_1) g_2;conj(g_2) -g_1]*[y(2*k+1);conj(y(2*k+2))];
    end
end