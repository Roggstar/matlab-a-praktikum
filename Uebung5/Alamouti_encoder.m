function X = Alamouti_encoder(s)

    X1 = s;
    X1(2:2:end) = -conj(X1(2:2:end));
    X2 = s;
    X2(1:2:end) = X2(2:2:end);
    X2(2:2:end) = conj(s(1:2:end));
    
    X = [X1;X2];
end

