close all;
clear;
clc;

laenge = 10000;
disp(['Länge des Bitvektors: ' num2str(laenge)]);

%Bitvektor
b = randi(2,1,laenge) - 1;

sigmaSqrd = logspace(-1,1,20);
BERplot = [];
BERplot_cplx = [];

for l=1:length(sigmaSqrd)
    n = sqrt(sigmaSqrd(l)) * randn(1,laenge);
    Varianz = var(n)
    y = modulation(b,'BPSK') + n;
    b_schlange = demodulation(y, 'BPSK');

    if(b == b_schlange)
        disp('BPSK: Alle richtig geschätzt');
    else
        disp('BPSK:Nicht alle richtig geschätzt');
    end

    BER = length(find((not(b == b_schlange))))/laenge;
    BERplot = [BERplot BER];

    n_R = sqrt(sigmaSqrd(l)) * randn(1,laenge/2);
    n_I = sqrt(sigmaSqrd(l)) * randn(1,laenge/2);
    n_cplx = n_R+ 1i*n_I;
    y_cplx = modulation(b,'QPSK') + n_cplx;
    b_schlange_cplx = demodulation(y_cplx,'QPSK');

    if(b == b_schlange_cplx)
        disp('QPSK: Alle richtig geschätzt');
    else
        disp('QPSK: Nicht alle richtig geschätzt');
    end

    BER_cplx = length(find((not(b == b_schlange_cplx))))/laenge;
    BERplot_cplx = [BERplot_cplx BER_cplx];

end

abscisse = 1./sigmaSqrd;

semilogy(10*log10(abscisse), BERplot);

grid on;
hold on;

semilogy(10*log10(abscisse), BERplot_cplx);

analyticalBER = 0.5*erfc(sqrt(0.5*abscisse));
semilogy(10*log10(abscisse),analyticalBER);

legend('BER BPSK', 'BER QPSK', 'BER analytical')
xlabel('E_b/\sigma² [dB]');
ylabel('BER');
