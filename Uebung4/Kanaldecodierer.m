function [b_dach] = Kanaldecodierer(b_dach_en,l,n,k)

    b_dach_en = reshape(b_dach_en,[n,l/n])';
    [h,G] = hammgen(n-k);
    s_matrix = mod(b_dach_en*h',2);
    b_dach = b_dach_en;
    for t = 1:size(s_matrix,1)
        if (s_matrix(t,:) == zeros(n-k))
            continue;   %Nullzeile = kein Fehler
        end      
        index = find(ismember(h',s_matrix(t,:),'rows'));
        b_dach(t,index) = not(b_dach(t,index));
    end
    b_dach = b_dach(:,4:7);
    b_dach = reshape(b_dach',[1,numel(b_dach)])';
end

