clear all;
close all;
clc;

%% Aufgabe 1
laenge = 10005;
b = randi([0 1],1,laenge)';

%%Aufgabe 2
n = 7;
k = 4;

[B,l] = Kanalcodierer(b,n,k);

%%Aufgabe 3
b_en = reshape(B',[1,numel(B)]);

%%Aufgabe 4
x = modulation(b_en,'BPSK');

%%Aufgabe 5
variance = 0.1;
noise = sqrt(variance) * randn(1,length(x));
y = x + noise;

%%Aufgabe 6
b_empf = demodulation(y, 'BPSK');

%%Aufgabe 7
l = length(b_empf);
b_dach = Kanaldecodierer(b_empf,l,n,k);
b_dach = b_dach(1:end-3);

if(isequal(b_dach, b))
    disp('Signal fehlerfrei uebertragen');
else
    disp('Waehrend der Uebertragung sind Fehler aufgetreten.');
end

%% Aufgabe 8
laenge = 10005;
b = randi([0 1],1,laenge)';

% Hamming encode
n = 7;
k = 4;
b_target = [b' zeros(1,k-mod(laenge,k))]';
laenge = length(b_target);

[B,l] = Kanalcodierer(b,n,k);
b_en = reshape(B',[1,numel(B)]);
x = modulation(b_en,'BPSK');

variances = logspace(-1,0,20);
BERs_ham = [];
BERs_noHam = [];

for s = 1:length(variances)
    %channel + noise
    variance = variances(s);
    noise = sqrt(variance) * randn(1,length(x));
    y = x + noise;
    b_empf = demodulation(y, 'BPSK');
    l = length(b_empf);
    
    %without Hamming decode    
    b_noHam = reshape(b_empf,[n,l/n])';    
    b_noHam = b_noHam(:,4:7);
    b_noHam = reshape(b_noHam',[1,numel(b_noHam)])';
    BER_noHam = length(find((not(b_target == b_noHam ))))/laenge;
    BERs_noHam = [BERs_noHam BER_noHam];
    
    %with Hamming decode
    b_dach = Kanaldecodierer(b_empf,l,n,k);    
    BER_ham = length(find((not(b_target == b_dach ))))/laenge;
    BERs_ham = [BERs_ham BER_ham];
end

semilogy(10*log10(1./variances), BERs_ham);
grid on;
hold on;
semilogy(10*log10(1./variances), BERs_noHam);
legend('BER with Hamming', 'BER without Hamming')
xlabel('E_b/\sigma² [dB]');
ylabel('BER');
