function [B,l] = Kanalcodierer(b,n,k)

    l = length(b);
    b = [b' zeros(1,k-mod(l,k))]';
    l = length(b);    
    b = reshape(b,[k,l/k])';
    [h,G] = hammgen(n-k);
    B = b*G;
    B = mod(B,2);
end

