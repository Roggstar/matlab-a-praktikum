function [x_moduliert] = modulation(b, modulationsArt)

    switch modulationsArt
        case 'BPSK'
            x_moduliert = b;
            x_moduliert(find(x_moduliert == 0)) = -1;
        case 'QPSK'
            for(k = 1:2:length(b))
                if b(k) == 0
                    if b(k+1) == 0
                        x_moduliert((k+1)/2) = 1+1i;%sqrt(2)+1i*sqrt(2);
                    else
                        x_moduliert((k+1)/2) = -1+1i;%-sqrt(2)+1i*sqrt(2);
                    end
                else
                    if b(k+1) == 0
                        x_moduliert((k+1)/2) = 1-1i;%sqrt(2)-1i*sqrt(2);
                    else
                        x_moduliert((k+1)/2) = -1-1i;%-sqrt(2)-1i*sqrt(2);
                    end
                end
            end
    end
end

