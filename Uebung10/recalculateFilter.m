function [] = recalculateFilter(handles)

    try
        global filterKoeffizienten samplingFrequency audioFile audioFiltered N L;
        filterWert = zeros(5,1);
        H = zeros(512,5);
        y = zeros(length(audioFile),5);
        filterWert(1) = get(handles.slider1,'Value');
        filterWert(2) = get(handles.slider2,'Value');
        filterWert(3) = get(handles.slider3,'Value');
        filterWert(4) = get(handles.slider4,'Value');
        filterWert(5) = get(handles.slider5,'Value');
        
        for i=1:length(filterWert)
            H(:,i) = 10^filterWert(i)*freqz(filterKoeffizienten{i},1);
        end
        H_sum = sum(H,2);
        plot(handles.frequenzgang,samplingFrequency/1000*[0:length(H)-1]/(2*length(H)),log10(abs(H_sum)));
        xlabel(handles.frequenzgang, 'Frequenz in kHz');
        ylim(handles.frequenzgang,[-1.1 1.1]);
        xlim(handles.frequenzgang,[0 samplingFrequency/2000]);
        
        for i=1:length(filterWert)
            y(:,i) = 10^filterWert(i)*filter(filterKoeffizienten{i},1,audioFile);
        end
        audioFiltered = sum(y,2);
        audioFiltered = audioFiltered/max(audioFiltered);
        
        lengthSeconds = length(audioFiltered)/samplingFrequency;
        timeAxis = linspace(0,lengthSeconds,length(audioFiltered));
        plot(handles.gefiltertesSignal,timeAxis,audioFiltered);
        xlabel(handles.gefiltertesSignal, 'Time in s');
        set(handles.gefiltertesSignal,'xlim', [0 lengthSeconds]);
        set(handles.gefiltertesSignal,'ylim', [-1 1]);
        
        axes(handles.gefiltertesSpektrum);
        spectrogram(audioFiltered,N,L,4096,'yaxis',samplingFrequency);
        %spectrogram(audioFiltered,N,L,4096,'yaxis',samplingFrequency)
        caxis([-150 -20]);
    catch
        msgbox('Error while recalculating filter.');
    end