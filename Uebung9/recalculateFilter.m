function [] = recalculateFilter(handles)

    try
        filter1 = get(handles.slider1,'Value');
        filter2 = get(handles.slider2,'Value');
        filter3 = get(handles.slider3,'Value');
        filter4 = get(handles.slider4,'Value');
        filter5 = get(handles.slider5,'Value');
        msgbox({['Filter1: ' num2str(filter1)]; ['Filter2: ' num2str(filter2)]; ['Filter3: ' num2str(filter3)];...
            ['Filter4: ' num2str(filter4)]; ['Filter5: ' num2str(filter5)]});
    catch
        msgbox('Error while recalculating filter.');
    end