function varargout = equalizer(varargin)

    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @equalizer_OpeningFcn, ...
                       'gui_OutputFcn',  @equalizer_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end

function equalizer_OpeningFcn(hObject, eventdata, handles, varargin)

    handles.output = hObject;
    guidata(hObject, handles);

function varargout = equalizer_OutputFcn(hObject, eventdata, handles) 

    varargout{1} = handles.output;
    clear all;
    clc;

function slider1_CreateFcn(hObject, eventdata, handles)

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function slider2_CreateFcn(hObject, eventdata, handles)

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function slider3_CreateFcn(hObject, eventdata, handles)

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function slider4_CreateFcn(hObject, eventdata, handles)

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function slider5_CreateFcn(hObject, eventdata, handles)

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
function loadAudioFileButton_Callback(hObject, eventdata, handles)

    [fileName,pathName] = uigetfile('.wav');
    try
        global audioFile samplingFrequency;
        [audioFile, samplingFrequency] = audioread([pathName fileName]);
        lengthSeconds = length(audioFile)/samplingFrequency;
        timeAxis = linspace(0,lengthSeconds,length(audioFile));
        plot(handles.originalSignal,timeAxis,audioFile);
        set(handles.originalSignal,'xlim', [0 lengthSeconds]);
    catch
        msgbox('Error while loading audio file.');
    end
    
    try
        N_exakt = floor(samplingFrequency * 25E-3);
        N = 2^nextpow2(N_exakt);
        L = N - N_exakt;
        axes(handles.originalSpektrum);
        spectrogram(audioFile,N,L,'yaxis');
    catch
        msgbox('Error while calculating the spectrogram.');
    end
    
    recalculateFilter(handles);

function resetButton_Callback(hObject, eventdata, handles)

    set(handles.slider1,'Value',0);
    set(handles.slider2,'Value',0);
    set(handles.slider3,'Value',0);
    set(handles.slider4,'Value',0);
    set(handles.slider5,'Value',0);
    recalculateFilter(handles);

function playButton_Callback(hObject, eventdata, handles)

    global audioFile samplingFrequency;
    try
        sound(audioFile,samplingFrequency);
    catch
        msgbox('No audio file loaded.');
    end 

function slider1_Callback(hObject, eventdata, handles)

    recalculateFilter(handles);
    
function slider2_Callback(hObject, eventdata, handles)

    recalculateFilter(handles);
    
function slider3_Callback(hObject, eventdata, handles)

    recalculateFilter(handles);
    
function slider4_Callback(hObject, eventdata, handles)

    recalculateFilter(handles);
    
function slider5_Callback(hObject, eventdata, handles)

    recalculateFilter(handles);
    
function playButton2_Callback(hObject, eventdata, handles)
