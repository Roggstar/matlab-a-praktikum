function encoded_cell = HuffmanEncoder(symbol_vec, probabilities_vec)

    %% Aufgabe 1
    cell_letter = cell(length(symbol_vec));
    Probability_Mtx = zeros(length(symbol_vec)-1,length(symbol_vec));
    
    %% Aufgabe 2
    cell_letter(1,:) = num2cell(symbol_vec);
    Probability_Mtx(1,:) = probabilities_vec;
    %bitvector = cell(length(symbol_vec));
    bitvector = cell(1,length(symbol_vec));
    
    %% Aufgabe 3
    for k=2:length(symbol_vec)
        
        %Aufgabenteil a
        [min1,index1] = min(Probability_Mtx(k-1,:));
        [min2,index2] = min(Probability_Mtx(k-1,[1:index1-1 index1+1:end]));
        if(index2>=index1)
            index2 = index2 + 1;
        end
        
        %Aufgabenteil b
        Probability_Mtx(k,:) = Probability_Mtx(k-1,:);
        cell_letter(k,:) = cell_letter(k-1,:);
        
        %Aufgabenteil c
        Probability_Mtx(k,index2) = min1 + min2;
        Probability_Mtx(k,index1) = 150;
        
        %Aufgabenteil d
        cell_letter(k,index2) = mat2cell([cell2mat(cell_letter(k,index2)) cell2mat(cell_letter(k,index1))],1);
        cell_letter(k,index1) = {[]};
    end
    
    %% Aufgabe 4
    for k=1:length(symbol_vec)-1
        row = length(symbol_vec)-k;  % row = 25 ... 1
        
        % siehe Aufgabe 3a, Minimum 1 und 2 finden
        [~,index1] = min(Probability_Mtx(row,:));
        [~,index2] = min(Probability_Mtx(row,[1:index1-1 index1+1:end]));
        if(index2>=index1)
            index2 = index2 + 1;
        end
        
        % eines der beiden Symbole muss bisher keinen Vektor haben, die
        % Konkatination ergibt also den Vektor des bisheringen Pfades
        oldVect = [cell2mat(bitvector(index1)) cell2mat(bitvector(index2))];

        bitvector(index1) = mat2cell([oldVect 0],1);
        bitvector(index2) = mat2cell([oldVect 1],1);        
    end
    
    encoded_cell(1,:) = num2cell(symbol_vec);
    encoded_cell(2,:) = num2cell(probabilities_vec);
    encoded_cell(3,:) = bitvector;