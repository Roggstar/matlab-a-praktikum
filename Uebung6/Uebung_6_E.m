clear;
close all;
clc;

%% Beispiel
bsp_symbols_vec = 'DECBA';
bsp_probabilities_vec = [.13 .07 .17 .23 .4];
Beispiel = HuffmanEncoder(bsp_symbols_vec, bsp_probabilities_vec);
openvar('Beispiel');

%% �bung
load('Prob_Letter.mat');
Uebung = HuffmanEncoder(symbols_vec, probabilities_vec);
openvar('Uebung');