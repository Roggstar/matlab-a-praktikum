function y = faltung(x,h)

    N = length(h) + length(x) - 1;
    y = zeros(1,N);
    for k = 1:N
        tempSum = 0;
        for m = 1:N
            if m > length(h)
                fac1=0;
            else
                fac1=h(m);
            end
            
            if (k-m+1) <= 0 || (k-m+1) > length(x)
                fac2 = 0;
            else
                fac2= x(k-m+1);
            end           
                
            tempSum = tempSum + fac1*fac2;
        end
        y(k) = tempSum;
    end
    
    subplot(2,2,1);
    stem(x);
    legend('x Plot');
    subplot(2,2,2);
    stem(h);
    legend('h Plot');
    subplot(2,2,3);
    stem(y);
    legend('y Plot');
    subplot(2,2,4);
    stem(conv(x,h));
    legend('conv Plot');
