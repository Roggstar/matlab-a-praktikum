clear;
close all;
clc;

x = [-1 2 1 0];
h = [-0.5 0.5 2 1 0];

faltungAusgabe = faltung(x,h)
convAusgabe = conv(x,h)

if (faltungAusgabe == convAusgabe)
    disp('faltung() und conv() stimmen �berein!')
else
    disp('faltung() und conv() stimmen nicht �berein!')
end

if(faltung(x,h) == faltung(h,x))
    disp('faltung() ist kommutativ!')
else
    disp('faltung() ist nicht kommutativ!')
end